const jwt = require("jsonwebtoken");

function getAnEmailFromReq(req, res, next) {
  const token = req.header("authorization").split(" ")[1];
  const decode = jwt.verify(token, "myVerySecretKey");
  res.locals.email = decode.email;
  return next();
}

module.exports = getAnEmailFromReq;
