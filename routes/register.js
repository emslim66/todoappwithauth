const express = require("express");
const router = express.Router();
const users = require("../services/users");
const bcrypt = require("bcrypt");

const map = require("../services/tasks");

module.exports = (params) => {
  router.post("/register", async (req, res) => {
    try {
      const { email, password } = req.body;

      const user = users.find((user) => user.email === email);
      if (!email || !password || password.length < 6 || user) {
        return res
          .status(400)
          .send(
            "Email field contains an invalid email, or the length of a password is less than 6 characters. Also, when any of the fields are missing."
          );
      }
      const hashedPassword = await bcrypt.hash(password, 10);
      users.push({ email: email, password: hashedPassword });

      res.status(200).send("A user was successfully registered.");
    } catch (err) {
      if (err) {
        res.status(505).send("Something went wrong on the server.");
      }
    }
  });
  return router;
};
