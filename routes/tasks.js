const express = require("express");

const router = express.Router();

const arr = require("../services/tasks");

const done = require("../services/done");

module.exports = () => {
  router.post("/", async (req, res) => {
    const { title, description } = req.body;
    if (!title || !description) {
      return res.status(400).send("Schema validation of the body failed.");
    }
    const email = req.app.locals.email;
    const check = arr.find((el) => {
      return el.title === title;
    });
    if (check) {
      return res.send("Task is already there");
    }
    arr.push({ email, title, description });
    res.status(200).send("A TODO task was successfully created for a user.");
  });

  router.get("/", (req, res) => {
    const arrForUserNoEmail = req.app.locals.userTasks.map((el) => {
      return { title: el.title, description: el.description };
    });
    res.send(arrForUserNoEmail);
  });

  router.post("/done", (req, res) => {
    const { title } = req.body;
    index = req.app.locals.userTasks.findIndex((el) => el.title === title);
    taskForDone = req.app.locals.userTasks.splice(index, 1);
    const indexOfArr = arr.findIndex((el) => el.title === title);
    if (indexOfArr === -1) {
      return res
        .status(404)
        .send(
          "A task with the provided title does not exist in the user`s to-do list."
        );
    }
    arr.splice(indexOfArr, 1);
    done.push(taskForDone[0]);
    console.log(arr);
    res.status(200).send('A user task was successfully marked as "done".');
  });

  router.get("/done", (req, res) => {
    const doneForUser = done.filter((el) => el.email === req.app.locals.email);
    const doneForUserNoEmail = doneForUser.map((el) => {
      return { title: el.title, description: el.description };
    });
    res.status(200).send(doneForUserNoEmail);
  });

  router.put("/:taskName", (req, res) => {
    const element = req.app.locals.userTasks.find(
      (el) => el.title === req.params["taskName"]
    );
    const index = req.app.locals.userTasks.findIndex(
      (el) => el.title === req.params["taskName"]
    );
    const { title, description } = req.body;
    const check = req.app.locals.userTasks.find((el) => {
      return el.title === title;
    });
    if (check) {
      return res
        .status(401)
        .send(
          "Schema validation failed, or a user tries to set a new title that already exists in the user`s list of tasks."
        );
    }
    const email = req.app.locals.email;
    req.app.locals.userTasks.splice(index, 1, { email, title, description });

    if (!element) {
      return res
        .status(404)
        .send("A task with such taskTitle is not found in the user`s tasks.");
    }

    const indexOfArray = arr.findIndex((el) => el === element);
    arr.splice(indexOfArray, 1, { email, title, description });
    res.status(200).send("A task was successfully updated.");
  });

  router.delete("/", (req, res) => {
    const { title } = req.body;

    if (!title || Object.keys(req.body).length > 1) {
      return res.status(400).send("Schema validation failed.");
    }

    const element = req.app.locals.userTasks.find((el) => el.title === title);
    if (!element) {
      return res
        .status(204)
        .send("a task with the provided title does not exist");
    }

    const indexOfArray = arr.findIndex((el) => el === element);
    const index = req.app.locals.userTasks.findIndex(
      (el) => el.title === title
    );
    req.app.locals.userTasks.splice(index, 1);
    arr.splice(indexOfArray, 1);
    res.status(204).send("A task was successfully deleted.");
  });

  return router;
};
