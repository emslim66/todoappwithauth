const express = require("express");
const TaskService = require("../services/taskService");
const router = express.Router();

const decoding = require("../decoding");

router.use("/", decoding);

const arr = require("../services/tasks");

const done = require("../services/done");

module.exports = () => {
  const taskService = new TaskService();
  router.post("/", async (req, res) => {
    const { title, description } = req.body;
    const email = res.locals.email;
    if (!title || !description) {
      return res.status(400).send("Schema validation of the body failed.");
    }
    if (await taskService.checkTask(title, email)) {
      return res.send("Task is already there");
    }
    await taskService.createTask(title, description, email);
    res.status(200).send("A TODO task was successfully created for a user.");
  });

  router.get("/", async (req, res) => {
    const tasksForUser = await taskService.filterArr(res.locals.email);
    res.send(tasksForUser);
  });

  router.post("/done", async (req, res) => {
    const { title } = req.body;
    const email = res.locals.email;
    if (await taskService.checkTaskDone(title, email)) {
      return res
        .status(404)
        .send(
          "A task with the provided title does not exist in the user`s to-do list."
        );
    }
    await taskService.addTaskToDone(email, title);
    res.status(200).send('A user task was successfully marked as "done".');
  });

  router.get("/done", async (req, res) => {
    const filteredDone = await taskService.filteredArrForDone(res.locals.email);
    res.status(200).send(filteredDone);
  });

  router.put("/:taskName", async (req, res) => {
    const { title, description } = req.body;
    const email = res.locals.email;
    const taskQuery = req.params["taskName"];
    if (!(await taskService.checkTask(taskQuery, email))) {
      return res
        .status(404)
        .send("A task with such taskTitle is not found in the user`s tasks.");
    }
    if (taskQuery === title) {
      return res
        .status(401)
        .send(
          "Schema validation failed, or a user tries to set a new title that already exists in the user`s list of tasks."
        );
    }
    await taskService.updateTask(taskQuery, title, description, email);
    res.status(200).send("A task was successfully updated.");
  });

  router.delete("/", async (req, res) => {
    const { title } = req.body;
    const email = res.locals.email;
    if (!title || Object.keys(req.body).length > 1) {
      return res.status(400).send("Schema validation failed.");
    }
    await taskService.deleteTask(title, email);
    res.status(204).send("Task sucessfully deleted");
  });

  return router;
};
