const express = require("express");
const jwt = require("jsonwebtoken");
const router = express.Router();
const users = require("../services/users");
const bcrypt = require("bcrypt");

module.exports = () => {
  router.post("/login", async (req, res) => {
    const password = req.body.password;
    const email = req.body.email;
    // if (!email || !password) {
    //   return res
    //     .status(400)
    //     .send(
    //       "Email field contains an invalid email, or any of the fields is missing, or a user with the provided credentials does not exist."
    //     );
    // }
    const user = users.find((user) => user.email === email);
    if (!user) {
      return res
        .status(400)
        .send(
          "Email field contains an invalid email, or any of the fields is missing, or a user with the provided credentials does not exist."
        );
    }
    bcrypt.compare(password, user.password, (err, resp) => {
      if (err) {
        return res.status(505).send("Something went wrong on the server.");
      }
      if (resp) {
        token = jwt.sign(user, "myVerySecretKey");
        return res
          .status(200)
          .json({ user: { email: user.email }, token: token });
      } else {
        return res
          .status(400)
          .send(
            "Email field contains an invalid email, or any of the fields is missing, or a user with the provided credentials does not exist."
          );
      }
    });
  });
  return router;
};
