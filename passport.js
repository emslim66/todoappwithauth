const passport = require("passport");
const passportJWT = require("passport-jwt");
const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;
const users = require("./services/users");

passport.use(
  new JWTStrategy(
    {
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
      secretOrKey: "myVerySecretKey",
    },
    function (jwtPayload, done) {
      const user = users.find((user) => user.email === jwtPayload.email);
      console.log(jwtPayload);
      if (user) {
        return done(null, user);
      } else {
        return done(
          new Error(
            "errorSchema validation of the body failed, or a user already has a task with the provided title."
          )
        );
      }
    }
  )
);

module.exports = passport;
