const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const routerForReg = require("./routes/register");
const routerForLog = require("./routes/login");
const routerForTask = require("./routes/tasksCopy");
const jwt = require("jsonwebtoken");

const passport = require("./passport");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use("/api/tasks", passport.authenticate("jwt", { session: false }));
app.use("/api/auth", routerForReg());
app.use("/api/auth", routerForLog());
app.use("/api/tasks", routerForTask());

app.listen(3000, () => {
  console.log("server is zver");
});
