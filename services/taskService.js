class TaskService {
  constructor() {
    this.newTasks = [];
    this.doneTasks = [];
  }
  async createTask(taskTitle, taskDesc, email) {
    this.newTasks.push({
      email: email,
      title: taskTitle,
      description: taskDesc,
    });
  }

  // async findIndex(email, taskTitle) {
  //   const index = this.newTasks.findIndex(
  //     (el) => el.title === taskTitle && el.email === email
  //   );
  //   return index;
  // }

  // async findIndexDone(email, taskTitle) {
  //   const index = this.doneTasks.findIndex(
  //     (el) => el.title === taskTitle && el.email === email
  //   );
  //   return index;
  // }

  async filterArr(email) {
    const filteredArr = this.newTasks.filter((el) => el.email === email);
    return filteredArr;
  }

  async filteredArrForDone(email) {
    const filteredArr = this.doneTasks.filter((el) => el.email === email);
    return filteredArr;
  }

  async addTaskToDone(email, title) {
    const index = this.newTasks.findIndex(
      (el) => el.title === title && el.email === email
    );
    const removedTask = this.newTasks.splice(index, 1);
    this.doneTasks.push(removedTask[0]);
  }

  async deleteTask(taskTitle, email) {
    const index = this.newTasks.findIndex(
      (el) => el.title === taskTitle && el.email === email
    );
    const indexForDone = this.doneTasks.findIndex(
      (el) => el.title === taskTitle && el.email === email
    );
    if (index !== -1) {
      this.newTasks.splice(index, 1);
    }
    if (indexForDone !== -1) {
      this.doneTasks.splice(indexForDone, 1);
    }
  }

  async checkTask(taskTitle, email) {
    const index = this.newTasks.findIndex(
      (el) => el.title === taskTitle && el.email === email
    );
    if (index !== -1) {
      return true;
    } else {
      return false;
    }
  }

  async checkTaskDone(taskTitle, email) {
    const index = this.doneTasks.findIndex(
      (el) => el.title === taskTitle && el.email === email
    );
    if (index !== -1) {
      return true;
    } else {
      return false;
    }
  }

  async updateTask(oldTitle, title, description, email) {
    const index = this.newTasks.findIndex(
      (el) => el.title === oldTitle && el.email === email
    );
    if (index !== -1) {
      return this.newTasks.splice(index, 1, { email, title, description });
    }
  }
}

module.exports = TaskService;
